// *******************************************
// IMAGE to TEXT and TEXT to IMAGE CONVERTERS
// *******************************************

(function() {

    "use strict";

    let base64String = "";

    function imageUploaded() {

        var file = document.getElementById("imageToTextFile").files[0];
        var reader = new FileReader();
        
        reader.onload = function () {
            base64String = reader.result.replace("data:", "")
                .replace(/^.+,/, "");
    
        var imageBase64String = base64String;
            read(imageBase64String);
        }
        if(file) {
            reader.readAsDataURL(file);
        }
    }
    
    function read(imageBase64String) {
        
        var texta = document.createElement("textarea");
        texta.setAttribute('id', 'texta');
        texta.setAttribute('class', 'form-control');
        texta.setAttribute('rows', '8');
        
        var textaParent = document.getElementById("textaLocation");
        textaParent.innerHTML = ''; // empty the eventual precedent textarea
        textaParent.appendChild(texta);
        
        var textarea = document.getElementById("texta");
        textarea.value = imageBase64String;
    }

    function base64ToImage() {
        var errorConvertToImage = document.getElementById("errorConvertToImage");
        errorConvertToImage.innerHTML = '';
        var errorMessage = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.indicateBase64Content') + '</p>';
        
        var imageBase64String = document.getElementById("texta2").value;
        
        if(imageBase64String) {

            if(!isBase64(imageBase64String)) {
                errorConvertToImage.innerHTML = errorMessage;
                return;
            }

            var image = document.createElement("img"),
                imageParent = document.getElementById("imageLocation");

            imageParent.innerHTML = ''; // empty the eventual precedent image
            image.id = "insertedImage";
            image.className = "insertedImage";
            image.src = 'data:image/png;base64,' + imageBase64String;
            imageParent.appendChild(image);
        } 
        else {
            errorConvertToImage.innerHTML = errorMessage;
            return;
        }
    }

    // Transform image to text
    document.getElementById('transformBtn').addEventListener('click', imageUploaded, false);

    // Transform text to image
    document.getElementById('base64ToImageBtn').addEventListener('click', base64ToImage, false);

})();
