
// *****
// XOR
// *****

(function() {

    "use strict";

    function generateSaltFinal() { 
        var saltForge = forge.util.bytesToHex(forge.random.getBytesSync(32)); // CryptoJS could also be used instead of Forge
        var saltIntermediate = document.getElementById('saltIntermediate').value;

        document.getElementById('saltFinal').value = common.xorHex(saltForge, saltIntermediate).toUpperCase();

    };

    // button to generate the key
    var buttonGenerateSaltFinal = document.getElementById('buttonGenerateSaltFinal');
    if(buttonGenerateSaltFinal){
        buttonGenerateSaltFinal.addEventListener('click', function(e){

            var saltIntermediate = document.getElementById('saltIntermediate').value;
            if('' != saltIntermediate) { 
                generateSaltFinal();
                buttonGenerateSaltFinal.classList.add('disabled'); // to avoid multiple click
            }
        });
    }

    function concatenate(resultConstructor, ...arrays) {
        let totalLength = 0

        for (const arr of arrays) totalLength += arr.length

        const result = new resultConstructor(totalLength)
        let offset = 0

        for (const arr of arrays) {
            result.set(arr, offset)
            offset += arr.length
        }

        return result
    }

    function randogram() {
        return crypto.getRandomValues(new Uint8Array(65536)) // max ArrayBufferView byte length
    }

    // https://stackoverflow.com/a/41550641
    function bin2hex(b) {
        return b.match(/.{4}/g).reduce(function(acc, i) {
            return acc + parseInt(i, 2).toString(16)
        }, '')
    }

    function genPixels() {
        return concatenate(Uint8Array, randogram(), randogram(), randogram(), randogram())
    }

    function drawRandogram() {
        const canvas = document.getElementById('randogram')
        if(canvas != null) {
            const ctx = canvas.getContext('2d', {alpha: false})
            const imgData = ctx.getImageData(0, 0, 512, 512)
            const pixels = genPixels()
            for (let i = 0; i < imgData.data.length; i += 4) {
                if (pixels[i / 4] % 2 === 0) {
                    imgData.data[i] = 0
                    imgData.data[i + 1] = 136
                    imgData.data[i + 2] = 255
                } else {
                    imgData.data[i] = 255
                    imgData.data[i + 1] = 255
                    imgData.data[i + 2] = 255
                }
    
            }
    
            ctx.putImageData(imgData, 0, 0)
            requestAnimationFrame(drawRandogram)
    
            return pixels
        }
    }

    function getEntropy() {
        const progressBar = document.getElementById('progressBar')
        const saltIntermediate = document.getElementById('saltIntermediate')
        const pixels = drawRandogram()

        let entropy = ""
        let neumann = []

        document.getElementById('randogram').onpointermove = function(e) {
            e.preventDefault()

            if (entropy.length < 256) {
                const x = Math.floor(e.offsetX)
                const y = Math.floor(e.offsetY)

                if (0 <= x && x < 512 && 0 <= y && y < 512) {
                    const p = 512 * y + x
                    let progress = 0

                    neumann.push(pixels[p] % 2)

                    // john von neumann randomness extractor
                    if (neumann.length === 2) {
                        if (neumann[0] !== neumann[1]) entropy += neumann[0]
                        neumann = []
                    }

                    progress = Math.floor(entropy.length/256 * 100)

                    if (progress > 100) progress = 100
                    else {
                        progressBar.style.width = progress + "%"
                        progressBar.innerText = progress + "%"
                    } // if progress > 100
                } // if 0 <= x < 512 && 0 <= y < 512
            } 
            else {
                saltIntermediate.value = bin2hex(entropy).toUpperCase();
                // context.clearRect(0, 0, canvas.width, canvas.height);
                var elem = document.querySelector('#randogram');
                elem.parentNode.removeChild(elem);
                
                // re-active button to generate final salt 
                var buttonGenerateSaltFinal = document.getElementById('buttonGenerateSaltFinal');
                if (buttonGenerateSaltFinal.classList.contains('disabled')) {
                    buttonGenerateSaltFinal.classList.remove('disabled');
                }

                return false;

            } // if entropy.length < 256
        } // onpointermove
        requestAnimationFrame(drawRandogram)

    } // getEntropy()
    getEntropy()
    
    // button to activate frame (canvas) to move mouse
    var buttonStopEntropyFrame = document.getElementById('buttonStopEntropyFrame');
    
    if(buttonStopEntropyFrame){
        buttonStopEntropyFrame.addEventListener('click', function(e){

            var el = document.getElementById('containerSalt');
            el.innerHTML = '';
            buttonStopEntropyFrame.classList.add('disabled');

        });
    }

})();
