-----------------------------------------
XENATX - v0.9.1
FR - Version alpha
EN - Alpha version
-----------------------------------------

FR - INFORMATION
----------------
Le XENATX est une application autonome de la marque XENATON (c). 
Cette application est utilisable dans un simple navigateur récent, sans besoin d'être connecté à Internet, et fait partie d'une suite d'outils, avec ou sans services en ligne associés, pour promouvoir et faciliter l'utilisation du chiffrement asymétrique, symétrique, OTP et des signatures.

ATTENTION, le XENATX fonctionne surtout avec les trois systèmes d'exploitation Windows, MacOS, Linux. Il y a des restrictions d'usage avec iOS sur iphone, comme l'impossibilité de jouer du son (morse). Il faut également dans certains cas utiliser une application tierce qui intègre une liseuse "html". Le navigateur par défaut type Safari ou même Firefox ne peut pas forcément faire fonctionner directement le XENATX correctement.
Pour Android, le fonctionnement est un peu similaire à celui pour l'iphone. Une app Android est en cours de développement.

COMMENT L'UTILISER ?
C'est très simple, une fois le dossier dézippé, cherchez le fichiez index.html dans le dossier au même niveau hiérarchique que le présent fichier README.txt et double-cliquez dessus. Votre navigateur par défaut va s'ouvrir et le XENATX est alors prêt à l'emploi, sans besoin de connexion Internet.

EMPREINTE - HASH
Vous pouvez obtenir l'empreinte SHA-512 du dossier zippé de cette version du XenaTx sur gitlab (https://gitlab.com/xenaton/xenatx/-/wikis/version), sur Telegram (t.me/xenaton_official) et sur le site xenaton.com

LICENCE
L'usage est gratuit et sans limitation. La licence a été maintenue "propriétaire" juste le temps d'un nécessaire affinage de cette jeune "application", encore en version alpha, avant de la faire vivre pleinement au sein de la communauté du logiciel libre. Dans le XENATX, seuls les éléments de design, d'ergonomie et d'implémentation du code sont réellement soumis à notre droit de propriété intellectuelle.
En tant qu'application js et html uniquement, le code est entièrement ouvert et volontairement non obfusqué pour faciliter les contrôles de sécurité et d'intégrité.
Plus d'explications dans la rubrique Outils et les deux onglets "Sécurité du code" et "XENATON".

BIBLIOTHEQUES OPEN SOURCE
Le XENATX comprend de nombreuses bibliothèques open source. Vous pourvez vérifier les empreintes de ces bibliothèques en utilisant les sources publiques indiquées ci-dessous des versions utilisées (également indiquées sur la page principale index.html - Section "Outils" et onglet "Sécurité du code")
Merci également de consulter leur licence respective.

GARANTIE
Aucune garantie logicielle. L'Utilisateur reconnaît et accepte que l'utilisation du Logiciel XENATX est à ses risques et périls. Le Logiciel et la documentation connexe sont fournis "TELS QUELS" sans aucune garantie d'aucune sorte et XENATON DÉCLINE EXPRESSÉMENT TOUTE GARANTIE, EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES IMPLICITES DE QUALITÉ MARCHANDE ET D'ADÉQUATION À UN USAGE PARTICULIER.

Pour toute demande ou suggestion, contactez-nous via xenaton.com

Merci de votre intérêt.

---

EN - INFORMATION
----------------
The XENATX is an autonomous app of brand XENATON (c).
It is usable in browser, without the need to be connected to the Internet, and is part of a suite of tools, with or without associated online services, to promote and facilitate the use of asymmetric, symmetric, OTP encryption and signatures.

ATTENTION, the XENATX mainly works with the three operating systems Windows, MacOS, Linux. There are usage restrictions with iOS on iphone, such as the impossibility of playing sound (morse code). In some cases, you also have to use a third-party application that includes an "html" reader. The default browser such as Safari or even Firefox cannot necessarily make the XENATX work correctly directly.
For Android, the operation is somewhat similar to that for the iphone. An Android app is under development.

HOW TO USE IT?
It's very simple, once you unzip the folder, find the index.html file in the folder at the same hierarchical level as this README.txt file and double-click it. Your default browser will open and the XENATX is then ready to use, without needing an Internet connection.

HASH
You can get the SHA-512 hash of the zipped folder of this version of XenaTx on gitlab (https://gitlab.com/xenaton/xenatx/-/wikis/version), on Telegram (t.me/xenaton_official) and on xenaton.com website

LICENCE
Use is free and without limitation. The license has been maintained as "proprietary" just for the necessary refinement of this young application, still in alpha version, before bringing it to full life within the free software community. In the XENATX, only the elements of design, ergonomics and implementation of the code are really subject to our intellectual property rights.
As a js and html only application, the code is fully open and intentionally unobfuscated to facilitate security and integrity checks.
More explanations in the Tools section and the two tabs "Security code" and "XENATON".

OPEN SOURCE LIBRARIES
The XENATX includes many open source libraries. Please verify checksum by using the indicated public sources regarding the versions used further below.
Please also have a look to their licence.

WARRANTY
No Software Warranty. Any user acknowledges and agrees that the use of the Software XENATX is at User’s sole risk. The Software and related documentation are provided “AS IS” and without any warranty of any kind and XENATON EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

For any request or suggestion contact us through xenaton.com

Thanks for reading and interest.


BIBLIOTHEQUES - LIBRARIES
-------------------------
bootstrap-5.2.0.js - https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/

bootstrap.min.js - https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/css/bootstrap.min.css - https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.9.0/font/bootstrap-icons.min.css

cryto-js-4.1.1.js - https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js

forge-1.3.1.js - https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js

i18next-21.8.14.js - https://www.i18next.com/ - https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js

jQuery-2.2.4.js - https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js

clipboard-2.0.10.js - https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.10/clipboard.min.js 

modernizr-3.6.0.js - https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)

openpgpjs-5.3.1.js - https://openpgpjs.org/ - https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js

qrcode.js - https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js

chroma-2.4.2.min.js - assets/javascripts/libs/chroma-light-2.4.2.min.js - https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js

js-cookie-2.1.4.min.js - assets/javascripts/libs/js-cookie-2.1.4.min.js - https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)

audio-decoder-adaptive-2.0.min.js - Décodage morse - Adaptive and morse decoder - SC Philips assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js - https://github.com/scp93ch/morse-pro/tree/master/src

mosaicVisualHash-1.0.1.js - assets/javascripts/libs/mosaicVisualHash-1.0.1.js - https://raw.githubusercontent.com/jfietkau/Mosaic-Visual-Hash/master/mosaicVisualHash.js


--------------------------------------------------
FR - Modification de cette bibliothèque jscwlib.js
--------------------------------------------------
jscwlib.js - Encodage morse - fkurz.net - assets/javascripts/libs/jscwlib.js - https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/)

ATTENTION ! - Très important de remettre les valeurs par défaut avant de faire le calcul et le contrôle des empreintes SHA-512. Voir les modifications ci-dessous.

Ligne 194
La fréquence a été mise à 563 à la place de 600.

Ligne 207
this.cgiurl = "https://cgi2.lcwo.net/cgi-bin/";
Suppression de l’url
this.cgiurl = "";

Ligne 310
this.oscillator.frequency.setValueAtTime(600, this.audioCtx.currentTime); // value in hertz
La valeur a été mise 563
this.oscillator.frequency.setValueAtTime(563, this.audioCtx.currentTime); // value in hertz

Ligne 553
ctx.fillStyle = 'rgb(200, 200, 200)';
Modification de la couleur :
ctx.fillStyle = 'rgb(234, 236, 239)’;

Ligne 540
ctx.strokeStyle = 'rgb(0, 0, 0)';
Modification de la couleur :
ctx.strokeStyle = 'rgb(11, 110, 253)’;

Ligne 1287
freq_label.innerHTML = "600 Hz";
La valeur a été mise à 563
freq_label.innerHTML = « 563 Hz";

---------------------------------------------
EN - Changing of this library jscwlib.js
---------------------------------------------
jscwlib.js - Encodage morse - fkurz.net - assets/javascripts/libs/jscwlib.js - https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/)

WARNING ! - Very important to set back the default values before hashing with SHA-512 and compare fingerprints. Have a look to the modified values below.

Line 194
Frequency value has been set to 563 instead of 600.

Line 207
this.cgiurl = "https://cgi2.lcwo.net/cgi-bin/";
Deletion of the url
this.cgiurl = "";

Line 310
this.oscillator.frequency.setValueAtTime(600, this.audioCtx.currentTime); // value in hertz
Value has been set to 563
this.oscillator.frequency.setValueAtTime(563, this.audioCtx.currentTime); // value in hertz

Line 553
ctx.fillStyle = 'rgb(200, 200, 200)';
Changing the color:
ctx.fillStyle = 'rgb(234, 236, 239)’;

Line 540
ctx.strokeStyle = 'rgb(0, 0, 0)';
Changing the color:
ctx.strokeStyle = 'rgb(11, 110, 253)’;

Line 1287
freq_label.innerHTML = "600 Hz";
Value has been set to 563
freq_label.innerHTML = « 563 Hz";